package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.bean.User;

@Service
public class LoginServiceImpl implements LoginService {

	private static final String REST_CONTACTO_URL = "http://localhost:8081/rest/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public boolean login(String username, String password) {
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		urlBuilder.path("login");
		
		Boolean response = restTemplate.postForObject(urlBuilder.toUriString(), user, Boolean.class);
		
		return response;
	}

}

package com.example.service;

import java.util.List;

import com.example.bean.Contacto;

public interface ContactoService {

	List<Contacto> obtenerContactos();
	Contacto agregarContacto(Contacto contacto);
	Contacto obtenerContacto(Integer id);
 	
}
